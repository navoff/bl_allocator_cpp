#include "bl_allocator.h"
#include <cstddef>

bl_allocator::bl_allocator()
{   
    for (size_t i = 0; i < pool_size - 1; ++i)
    {
        pool_[i].next_free_chunk = &pool_[i + 1];
    }
    pool_[pool_size - 1].next_free_chunk = nullptr;
    first_free_chunk_ = &pool_[0];
}

void* bl_allocator::allocate()
{
    if (first_free_chunk_ == nullptr)
    {
        // out of memory
        return nullptr;
    }

    mtx_.lock();

    void* result = first_free_chunk_->buffer;
    first_free_chunk_ = first_free_chunk_->next_free_chunk;

    mtx_.unlock();

    return result;
}

void bl_allocator::deallocate(void* buffer)
{
    if ((buffer < &pool_[0]) || (buffer >= &pool_[pool_size]))
    {
        // buffer is outside pool
        return;
    }

    mtx_.lock();

    const auto returned_chunk = 
        reinterpret_cast<chunk*>(static_cast<uint8_t*>(buffer) - offsetof(chunk, buffer));
    returned_chunk->next_free_chunk = first_free_chunk_;
    first_free_chunk_ = returned_chunk;

    mtx_.unlock();
}
