#pragma once
#include <cstdint>
#include <mutex>

class bl_allocator
{
public:
    /// Size of the single chunk, bytes
    static const size_t chunk_size = 100;

    /// Size of the pool, chunks
    static const size_t pool_size = 10;

    bl_allocator();

    /**
     * @brief Allocate one chunk
     * @return Pointer to the allocated buffer
     *         nullptr - if is out of memory
     */
    void* allocate();

    /**
     * @brief Deallocate chunk
     * @param buffer - pointer to release buffer
     */
    void deallocate(void* buffer);

private:
    struct chunk
    {
        chunk* next_free_chunk;
        uint8_t buffer[chunk_size];
    };

    chunk pool_[pool_size];
    chunk* first_free_chunk_;
    std::mutex mtx_;
};

