#include "pch.h"
#include "../bl_allocator/bl_allocator.h"

class bl_allocator_test : public testing::Test
{
protected:
    bl_allocator allocator_;
};

/**
 * @brief Checking capacity of the pool
 *
 * Get the total number of the chunk in pool (by allocatin all free space)
 * And compare this number with @ref bl_allocator::pool_size
 */
TEST_F(bl_allocator_test, max_chunk_size)
{
    size_t count = 0;
    while (allocator_.allocate() != nullptr)
    {
        count++;
    }
    EXPECT_EQ(count, bl_allocator::pool_size);
}

/**
 * @brief Checking allocate the all pool
 *
 * Allocate and fill all buffers by a sample
 * And then compare buffers with a sample from begin
 */
TEST_F(bl_allocator_test, alloc)
{
    uint8_t* buffers[bl_allocator::pool_size];

    // alloc all chunks and fill by sample
    for (size_t i = 0; i < bl_allocator::pool_size; i++)
    {
        buffers[i] = static_cast<uint8_t*>(allocator_.allocate());

        for (size_t k = 0; k < bl_allocator::chunk_size; k++)
        {
            buffers[i][k] = i + k;
        }
    }

    // compare with sample
    for (size_t i = 0; i < bl_allocator::pool_size; i++)
    {
        for (size_t k = 0; k < bl_allocator::chunk_size; k++)
        {
            if (buffers[i][k] != i + k)
            {
                FAIL();
            }
        }
    }

    SUCCEED();
}

/**
 * @brief Simple checking allocate and free
 * 
 * Check sequence:
 * - allocate and fill all chunks by a sample
 * - free only odd chunks
 * - allocate freed chunks again and fill it by another sample
 * - compare all chunks with a sample
 */
TEST_F(bl_allocator_test, alloc_and_free)
{
    uint8_t* buffers[bl_allocator::pool_size];

    // alloc all chunks and fill by sample
    for (size_t i = 0; i < bl_allocator::pool_size; i++)
    {
        buffers[i] = static_cast<uint8_t*>(allocator_.allocate());

        for (size_t k = 0; k < bl_allocator::chunk_size; k++)
        {
            buffers[i][k] = i + k;
        }
    }

    // free half of chunks (only odd)
    for (size_t i = 0, buf_idx = 1; i < bl_allocator::pool_size / 2; i++, buf_idx += 2)
    {
        allocator_.deallocate(buffers[buf_idx]);
    }

    // alloc freed half of chunks
    for (size_t i = 0, buf_idx = 1; i < bl_allocator::pool_size / 2; i++, buf_idx += 2)
    {
        buffers[buf_idx] = static_cast<uint8_t*>(allocator_.allocate());

        for (size_t k = 0; k < bl_allocator::chunk_size; k++)
        {
            buffers[buf_idx][k] = buf_idx - k;
        }
    }

    // compare with sample
    for (size_t i = 0; i < bl_allocator::pool_size; i++)
    {
        for (size_t k = 0; k < bl_allocator::chunk_size; k++)
        {
            if ((i & 0x01) == 0)
            {
                if (buffers[i][k] != static_cast<uint8_t>(i + k))
                {
                    FAIL();
                }
            }
            else
            {
                if (buffers[i][k] != static_cast<uint8_t>(i - k))
                {
                    FAIL();
                }
            }
        }
    }

    SUCCEED();
}